package com.github.lh.controller;

import com.github.lh.BaseControllerTest;
import org.junit.Test;
import org.springframework.test.annotation.Rollback;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

/**
 * @author <a href="mailto: 393803588@qq.com">刘涵(Hanl)</a>
 * By 2017/12/2
 */
public class UserControllerTest extends BaseControllerTest {

    @Test
    @Rollback(false)
    public void selectPageTest() throws Exception {
        normalEval(
                get("/user/selectPage")
                .param("current", "0")
                .param("username", "liuhan")
        );
    }
}
