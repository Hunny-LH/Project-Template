package com.github.lh.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializeFilterable;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.github.lh.BaseServiceTest;
import com.github.lh.domain.User;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * @author <a href="mailto:393803588@qq.com">HanL(liuhan3)</a>
 * @date 17-12-13
 */
public class UserServiceTest extends BaseServiceTest {

    @Autowired
    UserService userService;


    @Test
    public void select() {
        // 条件分页查询
        Page<User> page1 = new Page<>();
        page1.setCurrent(1);
        page1.setSize(5);
        EntityWrapper<User> query1 = new EntityWrapper<>(User.builder()
                .username("user4")
                .build());
        page1 = userService.selectPage(page1, query1);
        System.out.println(JSON.toJSONString(page1, SerializerFeature.PrettyFormat));

        // 全量分页查询
        Page<User> page2 = new Page<>(1, 5);
        page2 = userService.selectPage(page2);
        System.out.println(JSON.toJSONString(page2, SerializerFeature.PrettyFormat));
    }
}
