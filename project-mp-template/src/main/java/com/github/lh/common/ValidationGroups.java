package com.github.lh.common;

/**
 * 参数校验分组标记
 *
 * @author <a href="mailto:393803588@qq.com">HanL(liuhan3)</a>
 * @date 17-12-13
 */
public interface ValidationGroups {
    /**
     * 分组： 新增
     */
    interface Insert{}

    /**
     * 分组： 更新
     */
    interface Update{}

}
