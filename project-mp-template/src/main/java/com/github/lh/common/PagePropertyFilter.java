package com.github.lh.common;

import com.alibaba.fastjson.serializer.PropertyFilter;

import java.util.HashSet;
import java.util.Set;

/**
 * @author <a href="mailto:393803588@qq.com">HanL(liuhan3)</a>
 * @date 17-12-15
 */
public class PagePropertyFilter implements PropertyFilter {

    private final Set<String> serialField = new HashSet<>(5);

    {
        serialField.add("current");
        serialField.add("size");
        serialField.add("pages");
        serialField.add("total");
        serialField.add("records");
    }

    @Override
    public boolean apply(Object o, String s, Object o1) {
        if (serialField.contains(s)) {
            return true;
        }
        return false;
    }
}
