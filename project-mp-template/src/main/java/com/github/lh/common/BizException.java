package com.github.lh.common;

import lombok.Data;

/**
 * @author <a href="mailto: 393803588@qq.com">刘涵(Hanl)</a>
 * By 2017/12/2
 */
@Data
public class BizException extends RuntimeException {
    private ReturnCode code;
    private String messageCode;
    private Object[] args;

    public BizException(ReturnCode code, String messageCode, Object... args) {
        super(messageCode);
        this.code = code;
        this.messageCode = messageCode;
        this.args = args;
    }
}
