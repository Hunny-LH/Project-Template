package com.github.lh.common;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.Collection;
import java.util.Map;

/**
 * @author <a href="mailto: 393803588@qq.com">刘涵(Hanl)</a>
 * By 2017/12/2
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AssertUtils {

    public static void assertTrue(Boolean expression, ReturnCode code, String messageCode, Object... args) {
        if (!expression) {
            throw new BizException(code, messageCode, args);
        }
    }

    public static void assertFalse(Boolean expression, ReturnCode code, String messageCode, Object... args) {
        if (expression) {
            throw new BizException(code, messageCode, args);
        }
    }

    public static void assertNotNull(Object obj, ReturnCode code, String messageCode, Object... args) {
        if (obj == null) {
            throw new BizException(code, messageCode, args);
        }
    }

    public static void assertIsNull(Object obj, ReturnCode code, String messageCode, Object... args) {
        if (obj != null) {
            throw new BizException(code, messageCode, args);
        }
    }

    public static <T> void assertNotEmpty(Collection<T> collection, ReturnCode code, String messageCode, Object... args) {
        if (CollectionUtils.isEmpty(collection)) {
            throw new BizException(code, messageCode, args);
        }
    }

    public static void assertNotEmpty(Object[] objs, ReturnCode code, String messageCode, Object... args) {
        if (ObjectUtils.isEmpty(objs)) {
            throw new BizException(code, messageCode, args);
        }
    }

    public static <K, V> void assertNotEmpty(Map<K, V> map, ReturnCode code, String messageCode, Object... args) {
        if (CollectionUtils.isEmpty(map)) {
            throw new BizException(code, messageCode, args);
        }
    }

}
