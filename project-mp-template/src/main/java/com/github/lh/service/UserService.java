package com.github.lh.service;

import com.github.lh.domain.User;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author HanL
 * @since 2017-12-04
 */
public interface UserService extends IService<User> {

}
