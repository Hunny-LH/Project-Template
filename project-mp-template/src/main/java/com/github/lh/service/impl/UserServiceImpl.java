package com.github.lh.service.impl;

import com.github.lh.domain.User;
import com.github.lh.dao.UserDao;
import com.github.lh.service.UserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author HanL
 * @since 2017-12-04
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserDao, User> implements UserService {

}
