package com.github.lh.dao;

import com.github.lh.domain.User;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author HanL
 * @since 2017-12-04
 */
@Mapper
public interface UserDao extends BaseMapper<User> {

}
