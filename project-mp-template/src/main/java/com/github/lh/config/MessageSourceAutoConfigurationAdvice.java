package com.github.lh.config;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.MessageSourceAutoConfiguration;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;

/**
 * @author <a href="mailto:393803588@qq.com">HanL(liuhan3)</a>
 * @date 17-12-15
 */
@Configuration
@AutoConfigureAfter(MessageSourceAutoConfiguration.class)
public class MessageSourceAutoConfigurationAdvice {
    public MessageSourceAutoConfigurationAdvice(MessageSource messageSource) {
        ((ResourceBundleMessageSource) messageSource).setUseCodeAsDefaultMessage(true);
    }
}