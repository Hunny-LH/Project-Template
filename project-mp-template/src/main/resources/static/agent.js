document.querySelectorAll('*').forEach(n =>
n.addEventListener('click', e => {
    e.stopPropagation();
    console.log(e.target);

    e.returnValue = false;
    }
));

hookAjax({
    //拦截回调
    onreadystatechange:function(xhr){
        console.log("onreadystatechange called: %O",xhr)
    },
    onload:function(xhr){
        console.log("onload called: %O",xhr)
    },
    //拦截函数
    open:function(arg){
        console.log("open called: method:%s,url:%s,async:%s",arg[0],arg[1],arg[2])
        return true;
    }
});