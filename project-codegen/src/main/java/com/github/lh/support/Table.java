package com.github.lh.support;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto: 393803588@qq.com">刘涵(Hanl)</a>
 * By 2017/12/3
 */
@ToString
@EqualsAndHashCode(of = {"name"})
public class Table implements Serializable {
    @Getter
    private String name;
    @Getter
    private List<Column> columns;
    @Getter
    @Setter
    private Column primary;


    public Table(String name) {
        this.name = name;
        this.columns = new ArrayList<>();
    }

    public String getClassName() {
        return StringUtils.getCamelString(name, false);
    }

    public String getFieldName() {
        return StringUtils.getCamelString(name, true);
    }


    public List<String> getImports() {
        return columns.stream().map(Column::getDataType)
                .map(Class::getTypeName)
                .filter(typeName -> !typeName.startsWith("java.lang"))
                .distinct()
                .collect(Collectors.toList());
    }
}
