package com.github.lh.support;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author <a href="mailto: 393803588@qq.com">刘涵(Hanl)</a>
 * By 2017/12/3
 */
@Data
@Builder
@EqualsAndHashCode(of = {"columnName"})
public class Column implements Serializable {

    private String columnName;
    private Class<?> dataType;
    private Object columnDefault;
    private String desc;
    private int ordinal;
    private boolean isNullable;
    private boolean isPrimary;
    private boolean isUnique;

    public String getFieldName() {
        return StringUtils.getCamelString(columnName, true);
    }

}
