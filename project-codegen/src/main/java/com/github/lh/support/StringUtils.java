package com.github.lh.support;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.StringJoiner;

/**
 * @author <a href="mailto: 393803588@qq.com">刘涵(Hanl)</a>
 * By 2017/12/3
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class StringUtils {

    public static String getCamelString(String str, boolean ignoreFrist) {
        if (isEmpty(str)) {
            return null;
        }
        boolean toUp = !ignoreFrist;
        StringBuilder sb = new StringBuilder();
        str = str.replaceFirst("^\\w_", "");
        for (char c : str.toLowerCase().toCharArray()) {
            if (toUp) {
                sb.append(Character.toUpperCase(c));
                toUp = false;
            } else if (c == '_') {
                toUp = true;
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    public static String packageToPath(String packageName) {
        if (isEmpty(packageName)) {
            return null;
        }
        return packageName.replaceAll("\\.", "/");
    }

    public static String concatPath(String... parts) {
        StringJoiner joiner = new StringJoiner("/");
        for (String part : parts) {
            joiner.add(part);
        }
        return joiner.toString();
    }

    public static boolean isEmpty(String str) {
        return str == null || "".equals(str) || str.trim().length() == 0;
    }

    public static boolean notEmpty(String str) {
        return !isEmpty(str);
    }


    public static void main(String[] args) {
        String tableName = "Cs_User_info";
        System.out.println(getCamelString(tableName, true));

        System.out.println(packageToPath("com.github.lh"));

        System.out.println(concatPath("a", "com/github/lh", "User").concat(".java"));
    }
}
