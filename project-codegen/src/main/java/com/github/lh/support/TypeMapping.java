package com.github.lh.support;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto: 393803588@qq.com">刘涵(Hanl)</a>
 * By 2017/12/3
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TypeMapping {

    private static final Map<String, Class<?>> TYPE_MAPPING = new HashMap<>();

    static {
        TYPE_MAPPING.put("bit", Boolean.class);
        TYPE_MAPPING.put("mediumint", Integer.class);
        TYPE_MAPPING.put("smallint", Integer.class);
        TYPE_MAPPING.put("tinyint", Integer.class);
        TYPE_MAPPING.put("int", Integer.class);
        TYPE_MAPPING.put("integer", Integer.class);
        TYPE_MAPPING.put("bigint", Long.class);
        TYPE_MAPPING.put("decimal", BigDecimal.class);
        TYPE_MAPPING.put("double", Double.class);
        TYPE_MAPPING.put("float", Float.class);
        TYPE_MAPPING.put("binary", Byte[].class);
        TYPE_MAPPING.put("varbinary", Byte[].class);

        TYPE_MAPPING.put("datetime", LocalDateTime.class);
        TYPE_MAPPING.put("timestamp", LocalDateTime.class);
        TYPE_MAPPING.put("date", LocalDate.class);
        TYPE_MAPPING.put("time", LocalDate.class);

        TYPE_MAPPING.put("char", String.class);
        TYPE_MAPPING.put("varchar", String.class);
        TYPE_MAPPING.put("text", String.class);
        TYPE_MAPPING.put("tinytext", String.class);
        TYPE_MAPPING.put("mediumtext", String.class);
        TYPE_MAPPING.put("longtext", String.class);
    }

    public static Class<?> getType(String name) {
        return TYPE_MAPPING.getOrDefault(name, String.class);
    }
}
