package com.github.lh;

import com.github.lh.config.AutoGenerator;
import com.github.lh.config.Configuration;
import com.github.lh.config.GlobalConfig;
import com.github.lh.support.TableScanner;

/**
 * @author <a href="mailto: 393803588@qq.com">刘涵(Hanl)</a>
 * By 2017/12/6
 */
public class Main {

    public static void main(String[] args) {
        GlobalConfig gc = new GlobalConfig();
        gc.setOutputDir("/Users/hanl/Documents/workspace/ideaWork/lh-project-seed/project-test/src/main/java");
        gc.setGenDao(true);
        gc.setGenService(true);
        gc.setGenController(true);

        TableScanner ts = TableScanner.builder()
                .url("jdbc:mysql://192.168.3.134/test?charset=utf-8")
                .user("liuhan")
                .pass("liuhan")
                .build();

        Configuration config = Configuration.builder()
                .globalConfig(gc)
                .tableScanner(ts)
                .build();

        new AutoGenerator(config).run();
    }
}
