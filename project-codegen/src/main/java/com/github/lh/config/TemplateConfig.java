package com.github.lh.config;

import lombok.Data;

/**
 * @author <a href="mailto: 393803588@qq.com">刘涵(Hanl)</a>
 * By 2017/12/4
 */
@Data
public class TemplateConfig {

    private String entity = Const.TEMPLATE_ENTITY;
    private String dao = Const.TEMPLATE_DAO;
    private String service = Const.TEMPLATE_SERVICE;
    private String serviceImpl = Const.TEMPLATE_SERVICEIMPL;
    private String controller = Const.TEMPLATE_CONTROLLER;

}
