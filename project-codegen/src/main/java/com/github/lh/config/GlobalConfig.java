package com.github.lh.config;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author <a href="mailto: 393803588@qq.com">刘涵(Hanl)</a>
 * By 2017/12/4
 */
@ToString
public class GlobalConfig {
    @Getter
    @Setter
    private String outputDir = "";
    @Getter
    @Setter
    private String author = "HanL";
    @Getter
    @Setter
    private String email = "393803588@qq.com";
    @Getter
    @Setter
    private boolean genDao = false;
    @Getter
    @Setter
    private boolean genService = false;
    @Getter
    @Setter
    private boolean genController = false;
    @Getter
    @Setter
    private boolean isRestController = true;
    @Getter
    @Setter
    private String entitySuperClassName = null;
    @Getter
    @Setter
    private Class<?> entitySuperClass = null;
    @Getter
    @Setter
    private String daoSuperClassName = null;
    @Getter
    @Setter
    private Class<?> daoSuperClass = null;
    @Getter
    @Setter
    private String serviceSuperClassName = null;
    @Getter
    @Setter
    private Class<?> serviceSuperClass = null;
    @Getter
    @Setter
    private String serviceImplSuperClassName = null;
    @Getter
    @Setter
    private Class<?> serviceImplSuperClass = null;
    @Getter
    @Setter
    private String controllerSuperClassName = null;
    @Getter
    @Setter
    private Class<?> controllerSuperClass = null;

    @Setter
    private String daoName = "%sDao";
    @Setter
    private String serviceName = "%sService";
    @Setter
    private String serviceImplName = "%sServiceImpl";
    @Setter
    private String controllerName = "%sController";


    public String getDaoName(String entityName) {
        return String.format(daoName, entityName);
    }

    public String getServiceName(String entityName) {
        return String.format(serviceName, entityName);
    }

    public String getServiceImplName(String entityName) {
        return String.format(serviceImplName, entityName);
    }

    public String getControllerName(String entityName) {
        return String.format(controllerName, entityName);
    }
}
