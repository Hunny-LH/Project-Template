package com.github.lh.config;

/**
 * @author <a href="mailto: 393803588@qq.com">刘涵(Hanl)</a>
 * By 2017/12/4
 */
class Const {
    static final String TEMPLATE_ENTITY = "/templates/entity.vm";
    static final String TEMPLATE_DAO = "/templates/dao.vm";
    static final String TEMPLATE_SERVICE = "/templates/service.vm";
    static final String TEMPLATE_SERVICEIMPL = "/templates/serviceImpl.vm";
    static final String TEMPLATE_CONTROLLER = "/templates/controller.vm";

}
