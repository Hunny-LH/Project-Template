package com.github.lh.config;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author <a href="mailto: 393803588@qq.com">刘涵(Hanl)</a>
 * By 2017/12/4
 */
@ToString
public class PackageConfig {
    @Getter
    @Setter
    private String basePackage = "com.github.lh";
    @Setter
    private String entity = "entity";
    @Setter
    private String dao = "dao";
    @Setter
    private String service = "service";
    @Setter
    private String serviceImpl = "service.impl";
    @Setter
    private String controller = "controller";

    public String getEntity() {
        return joinedPackage(entity);
    }

    public String getDao() {
        return joinedPackage(dao);
    }

    public String getService() {
        return joinedPackage(service);
    }

    public String getServiceImpl() {
        return joinedPackage(serviceImpl);
    }

    public String getController() {
        return joinedPackage(controller);
    }

    private String joinedPackage(String part) {
        return String.format("%s.%s", basePackage, part);
    }
}
