package com.github.lh;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.*;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@Transactional
public class BaseControllerTest {

    protected MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        init();
    }

    public void init() {
    }

    public ResultActions perform(RequestBuilder builder) throws Exception {
        return this.mockMvc.perform(builder);
    }

    public MvcResult normalEval(RequestBuilder builder) throws Exception {
        return eval(builder, status().isOk());
    }

    public MvcResult eval(RequestBuilder builder, ResultMatcher... resultMatchers) throws Exception {
        ResultActions actions = perform(builder)
                .andDo(print());
        if (ObjectUtils.isEmpty(resultMatchers)) {
            actions.andExpect(status().isOk());
        } else {
            for (ResultMatcher matcher : resultMatchers) {
                actions.andExpect(matcher);
            }
        }
        return actions.andReturn();
    }
}
