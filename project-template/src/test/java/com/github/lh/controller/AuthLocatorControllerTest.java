package com.github.lh.controller;

import com.github.lh.BaseControllerTest;
import org.junit.Test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

/**
 * @author <a href="mailto: 393803588@qq.com">刘涵(Hanl)</a>
 * By 2017/12/2
 */
public class AuthLocatorControllerTest extends BaseControllerTest {

//    @Test
//    @Rollback
//    public void saveDuplicateUserTest() throws Exception {
//        User user = new User();
//        user.setUsername("liuhan2");
//
//        ReturnModel model = save(user);
//        assertThat(model.getCode(), Matchers.equalTo(ReturnCode.OK.code()));
//        User data = (User) model.getContent();
//        assertThat(data.getUsername(), equalTo(user.getUsername()));
//
//        model = save(user);
//        assertThat(model.getCode(), Matchers.equalTo(ReturnCode.OBJECT_EXISTS.code()));
//    }
//
//    private ReturnModel save(User user) throws Exception {
//        String resp = normalEval(
//                post("/user/save")
//                        .contentType(MediaType.APPLICATION_JSON_UTF8)
//                        .content(JSON.toJSONString(user))
//        ).getResponse().getContentAsString();
//
//        assertThat(resp, not(isEmptyString()));
//        return JSON.parseObject(resp, ReturnModel.class);
//    }

    @Test
    public void saveTest() throws Exception {
        normalEval(
                post("/authLocator/save")
                        .param("siteId", "16")
                        .param("loginUrl", "http://localhost:8080/login")
                        .param("loginType", "3")
                        .param("usernameLocator", "")
                        .param("usernameLocatorType", "2")
        );
    }

}
