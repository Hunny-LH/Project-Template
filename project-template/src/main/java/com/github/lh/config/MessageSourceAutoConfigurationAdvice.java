package com.github.lh.config;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.context.MessageSourceAutoConfiguration;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;

/**
 * @author <a href="mailto: 393803588@qq.com">刘涵(Hanl)</a>
 * By 2017/12/2
 */
@Configuration
@AutoConfigureAfter(MessageSourceAutoConfiguration.class)
public class MessageSourceAutoConfigurationAdvice {
    public MessageSourceAutoConfigurationAdvice(MessageSource messageSource) {
        ((ResourceBundleMessageSource) messageSource).setUseCodeAsDefaultMessage(true);
    }
}
