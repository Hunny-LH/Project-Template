package com.github.lh.common;

import org.springframework.http.HttpStatus;

import java.util.Arrays;

/**
 * @author <a href="mailto: 393803588@qq.com">刘涵(Hanl)</a>
 * By 2017/12/2
 */
public enum ReturnCode {
    /**
     * extends HttpStatus
     */
    OK(HttpStatus.OK),
    CREATED(HttpStatus.CREATED),
    BAD_REQUEST(HttpStatus.BAD_REQUEST),
    UNAUTHORIZED(HttpStatus.UNAUTHORIZED),
    FORBIDDEN(HttpStatus.FORBIDDEN),
    /**
     * custom defined
     */
    VALID_FAILED(4041),
    OBJECT_EXISTS(4042),
    OBJECT_OVERTIME(4043),
    OBJECT_NOT_FOUND(4044),
    OBJECT_INVALID(4045),
    /**
     *
     */
    UNKNOWN_ERROR(5001);

    private int code;

    ReturnCode(int code) {
        this.code = code;
    }

    ReturnCode(HttpStatus status) {
        this.code = status.value();
    }

    public int code() {
        return code;
    }

    public static ReturnCode valueOf(int code) {
        return Arrays.stream(values())
                .filter(returnCode -> returnCode.code == code)
                .findAny()
                .orElse(UNKNOWN_ERROR);
    }

}
