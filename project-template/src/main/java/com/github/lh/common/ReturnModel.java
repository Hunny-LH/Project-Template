package com.github.lh.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author <a href="mailto: 393803588@qq.com">刘涵(Hanl)</a>
 * By 2017/12/2
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReturnModel<T> implements Serializable {
    private int code;
    private String message;
    private T content;

    public static ReturnModel<?> ok() {
        return ok(ReturnCode.OK, null);
    }

    public static <T> ReturnModel<T> ok(T content) {
        return ok(ReturnCode.OK, content);
    }

    public static <T> ReturnModel<T> ok(ReturnCode returnCode, T content) {
        return ReturnModel.<T>builder()
                .code(returnCode.code())
                .message(returnCode.name())
                .content(content)
                .build();
    }

}
