package com.github.lh.service;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.Serializable;
import java.util.List;

/**
 * @author <a href="mailto: 393803588@qq.com">刘涵(Hanl)</a>
 * By 2017/12/1
 */
public interface BaseService<T, ID extends Serializable> {

    /**
     * save entity into support
     *
     * @param entity
     * @param <S>
     * @return
     */
    <S extends T> S save(S entity);

    /**
     * save entities into support
     *
     * @param entities
     * @param <S>
     * @return
     */
    <S extends T> List<S> save(Iterable<S> entities);

    /**
     * get by id
     *
     * @param id
     * @return
     */
    T getOne(ID id);

    /**
     * find one by query [using querydsl api]
     *
     * @param predicate
     * @return
     */
    T findOne(Predicate predicate);

    /**
     * delete by id
     *
     * @param id
     */
    void delete(ID id);

    /**
     * delete entity
     *
     * @param entity
     */
    void delete(T entity);

    /**
     * findAll with order [using querydsl api]
     *
     * @param predicate
     * @param orders
     * @return
     */
    Iterable<T> findAll(Predicate predicate, OrderSpecifier<?>... orders);

    /**
     * find by page [using querydsl api]
     *
     * @param predicate
     * @param pageable
     * @return
     */
    Page<T> findAll(Predicate predicate, Pageable pageable);
}
